const getConfig = require("../../getConfig");
const deployNow = require("../../api/deployNow");

module.exports.name = "deploy-now";

module.exports.description = "Take screenshots and deploy to Zeit Now";

module.exports.options = [
  { option: "-c --config <path>", description: "Path to config file" }
];

module.exports.action = async function action(cmd) {
  const config = getConfig(cmd);
  return deployNow(config);
};
