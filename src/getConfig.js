const path = require("path");
const fs = require("fs");
const puppeteer = require("puppeteer");

module.exports = function getConfig(args) {
  const configPath = args.config
    ? path.resolve(args.config)
    : path.join(process.cwd(), "sv.config.js");

  if (!fs.existsSync(configPath)) {
    throw new Error("Config file not found.");
  }
  return require(configPath)(puppeteer);
};
