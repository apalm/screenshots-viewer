const deviceDescriptors = require("puppeteer/DeviceDescriptors");

module.exports = [
  deviceDescriptors["iPhone 8"],
  deviceDescriptors["iPhone 8 landscape"],
  deviceDescriptors["iPhone X"],
  deviceDescriptors["iPhone X landscape"],
  deviceDescriptors["iPad"],
  deviceDescriptors["iPad landscape"],
  {
    name: "Desktop 1024x768",
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36",
    viewport: { width: 1024, height: 768 }
  },
  {
    name: "Desktop 1280x800",
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36",
    viewport: { width: 1280, height: 800 }
  },
  {
    name: "Desktop 1440x900",
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36",
    viewport: { width: 1440, height: 900 }
  }
];
