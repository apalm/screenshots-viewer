#!/usr/bin/env node

const commander = require("commander");
const commands = require("../src/cli/commands/index");

for (let x of commands) {
  const command = commander.command(x.name);
  if (x.usage) {
    command.usage(x.usage);
  }
  if (x.description) {
    command.description(x.description);
  }
  for (let option of x.options || []) {
    command.option(option.option, option.description);
  }
  // command.action(x.action);
  command.action(async (...y) => {
    try {
      await x.action(commander, ...y);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err.stack);
      process.exit(1);
    }
  });
}

commander.parse(process.argv);

if (process.argv.slice(2).length === 0) {
  commander.outputHelp();
}
