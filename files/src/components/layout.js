import * as React from "react";
import Head from "next/head";
import Router from "next/router";
import Link from "next/link";
import styles from "./layout.css";

export default props => {
  const title = props.title || "Screenshots Viewer";
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div className={styles.layout}>
        <header className={styles.header}>
          <Link href="/">
            <a>Screenshots Viewer</a>
          </Link>
          <Link
            href={{
              pathname: "/",
              query: { vid: props.vid, mode: "scenario" }
            }}
          >
            <a>By scenario</a>
          </Link>
          <Link
            href={{
              pathname: "/",
              query: { vid: props.vid, mode: "device" }
            }}
          >
            <a>By device</a>
          </Link>
          {props.variants.length <= 1 ? null : (
            <select
              value={props.vid}
              onChange={event => {
                Router.push({
                  pathname: "/",
                  query: { vid: event.target.value }
                });
              }}
            >
              {props.variants.map(variant => (
                <option key={variant.id} value={variant.id}>
                  {variant.name}
                </option>
              ))}
            </select>
          )}
        </header>
        {props.children}
      </div>
    </>
  );
};
