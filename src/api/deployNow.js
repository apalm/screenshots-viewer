const path = require("path");
const fs = require("fs");
const os = require("os");
const cp = require("child_process");
const fse = require("fs-extra");
const puppeteer = require("puppeteer");
const defaultDevices = require("../defaultDevices");

module.exports = async function main(config) {
  const { variants, scenarios, devices = defaultDevices } = config;

  const outputDirPath = path.join(
    os.tmpdir(),
    "screenshots-" + String(Date.now())
  );
  // eslint-disable-next-line
  console.log("Creating temporary workspace at " + outputDirPath + "...");
  fse.emptyDirSync(outputDirPath);

  const viewerFilesDirPath = path.join(__dirname, "..", "..", "files");

  const viewerPath = path.join(outputDirPath, "viewer");
  fse.copySync(viewerFilesDirPath, viewerPath);
  const screenshotsDirPath = path.join(viewerPath, "static", "img");
  fse.mkdirpSync(screenshotsDirPath);

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  let xs = [];

  for (let variant of variants) {
    const { baseUrl, cleanup } = await variant.setup();

    for (let scenario of scenarios) {
      for (let device of devices) {
        const screenshotPath = path.join(
          screenshotsDirPath,
          variant.name.toLowerCase().replace(/ /g, "_") +
            "_" +
            scenario.name.toLowerCase().replace(/ /g, "_") +
            "_" +
            device.name.toLowerCase().replace(/ /g, "_") +
            ".png"
        );

        xs.push({ variant, scenario, device, screenshotPath });

        await page.emulate(device);
        await page.goto(baseUrl + scenario.path);
        if (scenario.setup) {
          await scenario.setup(page);
        }
        await page.screenshot({ path: screenshotPath });
      }
    }

    await cleanup();
  }

  await browser.close();

  fs.writeFileSync(
    path.join(viewerPath, "data.json"),
    JSON.stringify(
      xs.map(x => ({
        ...x,
        variant: {
          id: x.variant.name.toLowerCase().replace(/ /g, "_"),
          name: x.variant.name
        },
        scenario: {
          ...x.scenario,
          id: x.scenario.name.toLowerCase().replace(/ /g, "_")
        },
        device: {
          ...x.device,
          id: x.device.name.toLowerCase().replace(/ /g, "_")
        },
        screenshotPath: path.relative(screenshotsDirPath, x.screenshotPath)
      })),
      null,
      2
    )
  );

  if (config.now) {
    const nowConfigPath = path.join(viewerPath, "now.json");
    let nowConfig = JSON.parse(fs.readFileSync(nowConfigPath), {
      encoding: "utf8"
    });
    nowConfig = { ...nowConfig, ...config.now };
    fs.writeFileSync(nowConfigPath, JSON.stringify(nowConfig, null, 2));
  }

  cp.spawnSync("now", [], { cwd: viewerPath, stdio: "inherit" });
};
