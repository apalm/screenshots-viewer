const getConfig = require("../../getConfig");
const local = require("../../api/local");

module.exports.name = "local";

module.exports.description = "Take screenshots and output to local directory";

module.exports.options = [
  { option: "-c --config <path>", description: "Path to config file" }
];

module.exports.action = async function action(cmd) {
  const config = getConfig(cmd);
  return local(config);
};
