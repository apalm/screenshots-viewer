const local = require("./local");
const deployNow = require("./deploy-now");

module.exports = [local, deployNow];
