import * as React from "react";
import Link from "next/link";
import _ from "lodash/fp";
import cx from "classnames";
import Layout from "../src/components/layout";
import layoutStyles from "../src/components/layout.css";
import styles from "./index.css";

const modeToLabel = {
  scenario: "Scenarios",
  device: "Devices"
};

export default class extends React.Component {
  static async getInitialProps(ctx) {
    const _data = require("../data.json");

    const query = ctx.query;

    const variants = _.uniqBy(x => x.id, _data.map(x => x.variant));
    const vid = query.vid || variants[0].id;

    const mode = query.mode || "scenario";
    const selectedId =
      query.id ||
      (mode === "scenario" ? _data[0].scenario.id : _data[0].device.id);
    const data =
      mode === "scenario"
        ? _data.filter(
            x => x.variant.id === vid && x.scenario.id === selectedId
          )
        : _data.filter(x => x.variant.id === vid && x.device.id === selectedId);
    const navData =
      mode === "scenario"
        ? _.uniqBy(
            x => x.id,
            _data.map(x => ({ id: x.scenario.id, name: x.scenario.name }))
          )
        : _.uniqBy(
            x => x.id,
            _data.map(x => ({
              id: x.device.id,
              name: x.device.name
            }))
          );

    return { query, variants, vid, mode, selectedId, data, navData };
  }

  render() {
    const {
      query,
      variants,
      vid,
      mode,
      selectedId,
      data,
      navData
    } = this.props;

    const getScreenshotLabel =
      mode === "scenario" ? x => x.device.name : x => x.scenario.name;
    const getScreenshotId =
      mode === "scenario" ? x => x.device.id : x => x.scenario.id;

    return (
      <Layout variants={variants} vid={vid}>
        <nav className={layoutStyles.nav}>
          <div>{modeToLabel[mode]}</div>
          <ul>
            {navData.map(({ id, name }, i) => (
              <li key={id}>
                <Link href={{ pathname: "/", query: { ...query, id } }}>
                  <a
                    className={cx(
                      selectedId === id && layoutStyles.navSelectedLink
                    )}
                  >
                    {name}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
        <main className={styles.main}>
          {data.map((x, i) => (
            <div id={getScreenshotId(x)} key={i}>
              <img
                src={"/static/img/" + x.screenshotPath}
                className={
                  x.device.viewport.height > x.device.viewport.width
                    ? styles.screenshotPortrait
                    : styles.screenshotLandscape
                }
              />
              <a href={"#" + getScreenshotId(x)}>{getScreenshotLabel(x)}</a>
            </div>
          ))}
        </main>
      </Layout>
    );
  }
}
