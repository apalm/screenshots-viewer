import * as React from "react";
import App, { Container } from "next/app";
import "./_global.css";

export default class extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <Component {...pageProps} />
      </Container>
    );
  }
}
